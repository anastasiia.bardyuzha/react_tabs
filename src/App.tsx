import './App.css';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const tabs = [
  {
    title: 'Tab 1',
    content: 'Some text 1',
  },
  {
    title: 'Tab 2',
    content: 'Some text 2',
  },
  {
    title: 'Tab 3',
    content: 'Some text 3',
  },
];

const App = () => (
  <div className="App">
    <h1>React tabs</h1>
  </div>
);

export default App;
