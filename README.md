## Task
1. Implement `Tabs` component displaying tabs
2. Implement `onTabSelected` callback triggered on tab change with tab data as an argument
3. Add `index` prop (default `0`)  to set an index of tab active at the beginning
4. (*) Add an ability to use Tabs like this
    ```tsx harmony
    <Tabs>
      <Tab title="Tab 2">
        Interesting Tab 2
      </Tab>

      <Tab title="Tab 3">
        <div>Hello!</div>
        <div>Hi!</div>
      </Tab>

      <Tab title="Tab 1">
        Some tsx
      </Tab>
    </Tabs>
    ```
